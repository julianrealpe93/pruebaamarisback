import * as http from "http";
import morgan from "morgan";
import express from "express";
import loadContainer from "./containers";
import { loadControllers } from "awilix-express";
import dotenv from "dotenv";
import 'reflect-metadata';
import swaggerUi from "swagger-ui-express";
import { swaggerConfig } from "./config/swagger";
const cors = require('cors');

async function starServer(){

    dotenv.config({
        override: true,
        path: `${__dirname}/.././.env`
    });

    const app = express();
    app.use(cors({
        origin: '*'
    }));
    const port = process.env.PORT;
    app.use(morgan("dev"));
    app.use(express.json());
    app.use(express.urlencoded({ extended: false }));
    app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerConfig));

    loadContainer(app);
    app.use(
        loadControllers("./components/**/*.controller.{ts,js}", { cwd: __dirname })
    );
    
    http.createServer(app);

    app.listen(port, () => {
        console.log(`Server on port ${port}`)
    });

}

starServer();