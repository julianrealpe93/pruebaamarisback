import { Request,Response } from "express"
import { route, POST, GET } from "awilix-express"
import CreateUserService from "./createUser.service";

/**
 * @swagger
 * tags:
 *  name: createUser
 *  description: API to manage your login processes.
 */
@route("/createUser")
export class CreateUserController{

    constructor(public CreateUserService: CreateUserService){};

    /**
     * @swagger
     * /createUser/roles:
     *  get:
     *      summary: find roles
     *      tags: [createUser]
     *      responses:
     *          200:
     *              description: roles found successful
     *          401: 
     *              description: roles found error
     */
    @route("/roles")
    @GET()
    async getRoles(req: Request, res: Response) {
        try {
            const login = await this.CreateUserService.getRoles();
            res.status(200).json(login);
        } catch (error: any) {
            res.status(401).json({ message: error.message });
        }
    };

    /**
     * @swagger
     * /createUser/createUser/:
     *  post:
     *      summary: create user
     *      tags: [createUser]
     *      requestBody:
     *          required: true
     *          content:
     *              application/json:
     *                  schema:
     *                      type: object
     *                      $ref: '#/components/schemas/CreateUserDTO'
     *      responses:
     *          200:
     *              description: user created successful
     *          401: 
     *              description: user create error
     */
    @route("/createUser")
    @POST()
    async createUser(req: Request, res: Response) {
        try {
            const login = await this.CreateUserService.createUser(req.body);
            res.status(200).json(login);
        } catch (error: any) {
            res.status(401).json({ message: error.message });
        }
    };

}