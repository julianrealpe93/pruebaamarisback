import CreateUserDTO from "./dto/createUser.dto";
import CreateUserImplRepository from "./repository/createUserImpl.repository";

export default class CreateUserService {

    constructor(public CreateUserImplRepository:CreateUserImplRepository){}

    async getRoles(){
        try {
            const roles: any = await this.CreateUserImplRepository.getRoles();
            
            return roles;

        } catch (error:any) {
            console.log("error: "+error.message);
            throw new Error("Roles could not be searched");
        }
    }

    async createUser(CreateUserDTO: CreateUserDTO){
        try {

            const user = await this.CreateUserImplRepository.findUser(CreateUserDTO.user);
            if(user && user?.length){
                return {error:"User existing"}
            }else{
                await this.CreateUserImplRepository.createUser(CreateUserDTO);
                return {ok:"ok"};
            }

        } catch (error:any) {
            console.log("error: "+error.message);
            throw new Error("User could not be create");
        }
    }

}