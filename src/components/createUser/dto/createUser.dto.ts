import { IsString, IsNotEmpty, IsNumber } from "class-validator"

/**
 * @swagger
 * components:
 *  schemas:
 *      CreateUserDTO:
 *          type: object
 *          properties:
 *              user:
 *                  type: string
 *                  description: user name for login
 *              password:
 *                  type: string
 *                  description: user password for login
 *              role:
 *                  type: integer
 *                  description: user role for login
 *          required:
 *              - user
 *              - password
 *              - role
 *          example:
 *              user: alan123
 *              password: alan12345
 *              role: 1
 */
export default class CreateUserDTO{

    @IsNotEmpty()
    @IsString()
    user!: string;

    @IsNotEmpty()
    @IsString()
    password!: string;

    @IsNotEmpty()
    @IsNumber()
    role!: number;

}