import CreateUserDTO from "../dto/createUser.dto";

export default interface CreateUserRepository{

    getRoles:(CreateUserDTO: CreateUserDTO) => any;
    findUser:(user: string) => any;
    createUser:(CreateUserDTO: CreateUserDTO) => any;

}