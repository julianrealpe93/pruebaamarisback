import CreateUserRepository from "./createUser.repository";
import { pruebaamarisbackDB } from "../../../config/pg-promise"
import CreateUserDTO from "../dto/createUser.dto";

export default class CreateUserImplRepository implements CreateUserRepository{

    constructor(){}

    async getRoles(){

        const pool = pruebaamarisbackDB;
        const sql = `
            SELECT
                *
            FROM
                public.role
        `;

        return await pool.any(sql).then(data => {
            return data
        }).catch(error => {
            throw new Error(error.message);
        });

    }

    async findUser(
        user: string
    ){

        const pool = pruebaamarisbackDB;
        const sql = `
            SELECT
                id
            FROM
                public.user
            WHERE
                user_name = '${user}'
        `;

        return await pool.any(sql).then(data => {
            return data
        }).catch(error => {
            throw new Error(error.message);
        });

    }

    async createUser(CreateUserDTO: CreateUserDTO){

        const pool = pruebaamarisbackDB;
        const sql = `
            INSERT INTO public."user"(
                user_name, user_password, id_role)
            VALUES ('${CreateUserDTO.user}',
                    '${CreateUserDTO.password}',
                    ${CreateUserDTO.role});
        `;

        return await pool.none(sql).then(data => {
            return data
        }).catch(error => {
            throw new Error(error.message);
        });

    }

}