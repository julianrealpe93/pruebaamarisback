import { Request,Response } from "express"
import { route, POST, GET } from "awilix-express"
import CreateWebService from "./createWeb.service";

/**
 * @swagger
 * tags:
 *  name: createWeb
 *  description: API to manage your login processes.
 */
@route("/createWeb")
export class CreateWebController{

    constructor(public CreateWebService: CreateWebService){};

    /**
     * @swagger
     * /createWeb/getElements:
     *  get:
     *      summary: find Elements
     *      tags: [createWeb]
     *      responses:
     *          200:
     *              description: Elements found successful
     *          401: 
     *              description: Elements found error
     */
    @route("/getElements")
    @GET()
    async getElements(req: Request, res: Response) {
        try {
            const elements = await this.CreateWebService.getElements();
            res.status(200).json(elements);
        } catch (error: any) {
            res.status(401).json({ message: error.message });
        }
    };

    /**
     * @swagger
     * /createWeb/createWeb/:
     *  post:
     *      summary: create user
     *      tags: [createWeb]
     *      requestBody:
     *          required: true
     *          content:
     *              application/json:
     *                  schema:
     *                      type: object
     *                      $ref: '#/components/schemas/CreateWebDTO'
     *      responses:
     *          200:
     *              description: web page created successful
     *          401: 
     *              description: web page create error
     */
    @route("/createWeb")
    @POST()
    async createWeb(req: Request, res: Response) {
        try {
            const login = await this.CreateWebService.createWeb(req.body);
            res.status(200).json(login);
        } catch (error: any) {
            res.status(401).json({ message: error.message });
        }
    };

    /**
     * @swagger
     * /createWeb/getWebs/:
     *  post:
     *      summary: get webs
     *      tags: [createWeb]
     *      requestBody:
     *          required: true
     *          content:
     *              application/json:
     *                  schema:
     *                      type: object
     *                      $ref: '#/components/schemas/getWebDTO'
     *      responses:
     *          200:
     *              description: web page found successful
     *          401: 
     *              description: web page found error
     */
    @route("/getWebs")
    @POST()
    async getWebs(req: Request, res: Response) {
        try {
            const webs = await this.CreateWebService.getWebs(req.body);
            res.status(200).json(webs);
        } catch (error: any) {
            res.status(401).json({ message: error.message });
        }
    };

}