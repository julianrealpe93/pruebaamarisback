import CreateWebDTO from "./dto/createWeb.dto";
import GetWebDTO from "./dto/getWebs.dto";
import CreateWebImplRepository from "./repository/createWebImpl.repository";

export default class CreateWebService {

    constructor(public CreateWebImplRepository:CreateWebImplRepository){}

    async getElements(){
        try {
            const elements: any = await this.CreateWebImplRepository.getElements();
            
            return elements;

        } catch (error:any) {
            console.log("error: "+error.message);
            throw new Error("Elements could not be searched");
        }
    }

    async createWeb(CreateWebDTO: CreateWebDTO){
        try {
            const webPage = await this.CreateWebImplRepository.createWeb(CreateWebDTO.user, CreateWebDTO.pageName);
            if(webPage && webPage.length){
                
                const sections: any = JSON.parse(CreateWebDTO.section);
                for (const section of sections) {
                    await this.CreateWebImplRepository.createWebSection(section.html, section.text, webPage[0].id, section.section);
                }
            }else{
                throw new Error("No se pudo crear la web page")
            }
            
            return {ok:"ok"};


        } catch (error:any) {
            console.log("error: "+error.message);
            throw new Error("web page could not be created");
        }
    }

    async getWebs(GetWebDTO: GetWebDTO){
        try {
            const webPages = await this.CreateWebImplRepository.getWebs(GetWebDTO.user);            
            return webPages;
        } catch (error:any) {
            console.log("error: "+error.message);
            throw new Error("web page could not be created");
        }
    }

}