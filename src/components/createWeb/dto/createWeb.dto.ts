import { IsString, IsNotEmpty, IsNumber } from "class-validator"

/**
 * @swagger
 * components:
 *  schemas:
 *      CreateWebDTO:
 *          type: object
 *          properties:
 *              user:
 *                  type: string
 *                  description: user name
 *              pageName:
 *                  type: string
 *                  description: name of the page
 *              section:
 *                  type: string
 *                  description: data of the sections
 *          required:
 *              - user
 *              - password
 *              - role
 *          example:
 *              user: alan123
 *              pageName: tops10
 *              section: "[{section:12,html:h2,text:introduccion a los tops}]"
 */
export default class CreateWebDTO{

    @IsNotEmpty()
    @IsString()
    user!: string;

    @IsNotEmpty()
    @IsString()
    pageName!: string;

    @IsNotEmpty()
    @IsString()
    section!: string;

}