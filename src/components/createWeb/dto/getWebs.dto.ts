import { IsString, IsNotEmpty, IsNumber } from "class-validator"

/**
 * @swagger
 * components:
 *  schemas:
 *      getWebDTO:
 *          type: object
 *          properties:
 *              user:
 *                  type: string
 *                  description: user name
 *          required:
 *              - user
 *          example:
 *              user: alan123
 */
export default class GetWebDTO{

    @IsNotEmpty()
    @IsString()
    user!: string;

}