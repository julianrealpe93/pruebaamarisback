export default interface CreateWebRepository{
    getElements:() => any;
    createWeb:(user: string, pageName: string) => any;
    createWebSection:(html: string, text: string, webPageId: number, section: number) => any;
    getWebs: (user: string) => any;
}