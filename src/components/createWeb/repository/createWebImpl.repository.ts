import CreateWebRepository from "./createWeb.repository";
import { pruebaamarisbackDB } from "../../../config/pg-promise"

export default class CreateWebImplRepository implements CreateWebRepository{

    constructor(){}

    async getElements(){

        const pool = pruebaamarisbackDB;
        const sql = `
            SELECT
                name,
                html || '-' || type_of_input as confg
            FROM
                public.element
        `;

        return await pool.any(sql).then(data => {
            return data
        }).catch(error => {
            throw new Error(error.message);
        });

    }

    async createWeb(user: string, pageName: string){

        const pool = pruebaamarisbackDB;
        const sql = `
            INSERT INTO public."webPage"(
                name, creater)
            VALUES ('${pageName}', '${user}');
            SELECT max(id) as id from public."webPage";
        `;

        return await pool.any(sql).then(data => {
            return data
        }).catch(error => {
            throw new Error(error.message);
        });

    }

    async createWebSection(html: string, text: string, webPageId: number, section: number){

        const pool = pruebaamarisbackDB;
        const sql = `
            INSERT INTO public."webPageSection"(
                html, text, webpage_id, section)
            VALUES ('${html}', '${text}', ${webPageId}, ${section});
        `;

        return await pool.any(sql).then(data => {
            return data
        }).catch(error => {
            throw new Error(error.message);
        });

    }

    async getWebs(user: string){

        const pool = pruebaamarisbackDB;
        const sql = `
            SELECT
                *
            FROM
                public."webPage"
        `;

        return await pool.any(sql).then(data => {
            return data
        }).catch(error => {
            throw new Error(error.message);
        });

    }

}