import { apiStreamDB } from "../../config/pg-promise"

export async function endTransaction(commit: boolean = true) {
    
  const pool = apiStreamDB;
  await pool.any(commit?"rollback;":"commit;").catch(error => {
    throw new Error(error.message);
  });

}