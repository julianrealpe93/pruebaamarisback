import { IsString, IsNotEmpty } from "class-validator"

/**
 * @swagger
 * components:
 *  schemas:
 *      LoginDTO:
 *          type: object
 *          properties:
 *              user:
 *                  type: string
 *                  description: user name for login
 *              password:
 *                  type: string
 *                  description: user password for login
 *          required:
 *              - user
 *              - password
 *          example:
 *              user: alan123
 *              password: alan12345
 */
export default class LoginDTO{

    @IsNotEmpty()
    @IsString()
    user!: string;

    @IsNotEmpty()
    @IsString()
    password!: string;

}