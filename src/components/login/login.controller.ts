import { Request,Response } from "express"
import { route, POST, GET } from "awilix-express"
import LoginService from "./login.service";

/**
 * @swagger
 * tags:
 *  name: login
 *  description: API to manage your login processes.
 */
@route("/login")
export class LoginController{

    constructor(public loginService: LoginService){};

    /**
     * @swagger
     * /login/:
     *  post:
     *      summary: have loguin
     *      tags: [login]
     *      requestBody:
     *          required: true
     *          content:
     *              application/json:
     *                  schema:
     *                      type: object
     *                      $ref: '#/components/schemas/LoginDTO'
     *      responses:
     *          200:
     *              description: login successful
     *          401: 
     *              description: login error
     */
    @route("/")
    @POST()
    async login(req: Request, res: Response) {
        try {
            const login = await this.loginService.login(req.body);
            res.status(200).json(login);
        } catch (error: any) {
            res.status(401).json({ message: error.message });
        }
    };

}