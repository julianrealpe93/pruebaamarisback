import LoginDTO from "./dto/login.dto";
import LoginImplRepository from "./repository/loginImpl.repository";
import { endTransaction } from "../global/transaction"
import { generateToken } from "../middleware/jwt"
import dotenv from "dotenv";

export default class LoginService {

    constructor(public loginImplRepository:LoginImplRepository){}

    async login(form:LoginDTO){
        try {
            const userData: any = await this.loginImplRepository.getUserData(form.user);
            
            if((!userData || userData.length == 0) || form.password!==userData[0].user_password){
                return {error:"Incorrect credentials"};
            }else {
                const login = generateToken(userData[0]);
                
                return {"userData": userData[0], "token": login};
            }

        } catch (error:any) {
            console.log("error: "+error.message);
            throw new Error("No se pudo iniciar la sesion");
        }
    }

}