import LoginRepository from "./login.repository";
import { pruebaamarisbackDB } from "../../../config/pg-promise"
import pgPromise from "pg-promise";

export default class LoginImplRepository implements LoginRepository{

    constructor(){}

    async getUserData(
        user: string
    ){

        const pool = pruebaamarisbackDB;
        const sql = `
            SELECT
                *
            FROM
                public.user
            WHERE
                user_name = '${user}'
        `;

        return await pool.any(sql).then(data => {
            return data
        }).catch(error => {
            throw new Error(error.message);
        });

    }

}