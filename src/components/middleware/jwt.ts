import jwt from "jsonwebtoken";
import crypto from "crypto";
import dotenv from "dotenv";

export function generateToken(userObj: any){
    
    dotenv.config({
        path: `${__dirname}/.././.env`
    });
    return jwt.sign(userObj, process.env.TOKEN_SECRET as string, { expiresIn: '1800s' });

}

/**
 * @swagger
 * components:
 *  securitySchemes:
 *      bearerAuth:
 *          type: http
 *          scheme: bearer
 *          bearerFormat: JWT
 */
export function authenticateToken(req: any, res: any, next: any) {

    const authHeader = req.headers['authorization']
    const token = authHeader && authHeader.split(' ')[1]
  
    if (token == null) return res.sendStatus(401)
  
    jwt.verify(token, process.env.LOGUIN_TOKEN_SECRET as string, (err: any, userData: any) => {
      console.log(err)
  
      if (err) return res.sendStatus(403)
  
      req.userData = userData;
  
      next();
    })
}