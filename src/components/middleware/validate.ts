import { RequestHandler } from "express";
import { instanceToPlain, plainToInstance } from "class-transformer";
import { validate, ValidationError } from "class-validator";
import { NextFunction, Request, Response } from "express";

export function DTOvalidate(dto: any, skipMissingProperties = false): RequestHandler {
    return (req: Request, res: Response, next: NextFunction) => {
        Object.assign(dto, req.body);
        const dtoObj: any = instanceToPlain(dto, { excludeExtraneousValues: true, exposeUnsetFields: false, });
        validate(dtoObj, { skipMissingProperties }).then(
            (errors: ValidationError[]) => {
                if (errors.length > 0) {
                    const dtoErrors = errors.map((error: ValidationError) => (Object as any).values(error.constraints)).join(", ");
                    next(res.status(401).json({ error: dtoErrors }));
                } else {
                    next();
                }
            }
        );
    }
}