import dotenv from "dotenv";

dotenv.config({
    path: `${__dirname}/../../.env`
});

const pruebaamarisback = {
    host: process.env.testAmarisDB_host as string,
    port: parseInt(process.env.testAmarisDB_port as string),
    database: process.env.testAmarisDB_name as string,
    user: process.env.testAmarisDB_user as string,
    password: process.env.testAmarisDB_pass as string,
};

export { pruebaamarisback }