import { pruebaamarisback } from "./db";
import pgPromise from "pg-promise";

const pgp = pgPromise();
const pruebaamarisbackDB = pgp(pruebaamarisback);

export { pruebaamarisbackDB }