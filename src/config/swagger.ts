import swaggerJsdoc from "swagger-jsdoc";
import dotenv from "dotenv";

dotenv.config({
    override: true,
    path: `${__dirname}/../../.env`
});

const options = {
    failOnErrors: true, // Whether or not to throw when parsing errors. Defaults to false.
    definition: {
        openapi: '3.0.0',
        info: {
            title: 'pruebaAmaris',
            version: '1.0.0',
        },
        servers: [
            {
                url: `http://localhost:${process.env.PORT}`
            }
        ],
    },
    apis: [
        './src/components/middleware/jwt.{js,ts}',
        './src/components/**/dto/*.dto.{js,ts}',
        './src/components/**/*.controller.{js,ts}'
    ],
};

const swaggerConfig = swaggerJsdoc(options);

export { swaggerConfig }