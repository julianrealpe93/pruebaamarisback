import express from "express";
import { createContainer,asClass } from "awilix";
import { scopePerRequest } from "awilix-express";
import LoginService from "./components/login/login.service";
import LoginImplRepository from "./components/login/repository/loginImpl.repository";
import CreateUserService from "./components/createUser/createUser.service";
import CreateUserImplRepository from "./components/createUser/repository/createUserImpl.repository";
import CreateWebService from "./components/createWeb/createWeb.service";
import CreateWebImplRepository from "./components/createWeb/repository/createWebImpl.repository";

export default (app: express.Application): void => {
    const container = createContainer({
        injectionMode: "CLASSIC"
    });

    container.register({
        //Service
        loginService: asClass(LoginService).scoped(),
        CreateUserService: asClass(CreateUserService).scoped(),
        CreateWebService: asClass(CreateWebService).scoped(),
        //Repository
        loginImplRepository: asClass(LoginImplRepository).scoped(),
        CreateUserImplRepository: asClass(CreateUserImplRepository).scoped(),
        CreateWebImplRepository: asClass(CreateWebImplRepository).scoped(),
    });

    app.use(scopePerRequest(container));

};